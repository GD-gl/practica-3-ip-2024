-- 1
f :: Integer -> Integer
f 1 = 8
f 4 = 131
f 16 = 16

g :: Integer -> Integer
g 8 = 16
g 16 = 4
g 131 = 1

h :: Integer -> Integer
h x = f (g x)

k :: Integer -> Integer
k x = g (f x)

main = do

    -- print (f 1)
    -- print (f 4)
    -- print (f 16)

    -- print (g 8)
    -- print (g 16)
    -- print (g 131)

    print (h 8)
    print (k 4)
