-- Obs: a*a debe ser multiplo de a*b para que exista un k (negativo)
-- Obs 2: puedo simplificar la operación módulo, a debe ser múltiplo de b (pero voy a dejar mi implementación inicial)
estanRelacionados :: Integer -> Integer -> Bool
estanRelacionados 0 0 = undefined
estanRelacionados a b = mod (a * a) (a * b) == 0
-- estanRelacionados a b = mod a b == 0

main = do
    print (estanRelacionados 8 2) -- True
    print (estanRelacionados 7 3) -- False
