-- voy a usar un par de funciones definidas por Haskell:
-- fst, snd, abs, mod, even
-- la implementación puntual de cada una es muy simple:
-- fst, snd simplemente me dan el primer o segundo elemento de una tupla
-- abs retorna a <=> a >= 0 || -a <=> a < 0
-- mod es el resto de la división escrito como dividendo = divisor * cociente + resto
-- even es "n `mod` 2 == 0", o sea se fija si el número es par o no :)

prodInt :: (Float, Float) -> (Float, Float) -> Float
prodInt x y = fst x * fst y + snd x * snd y

todoMenor :: (Float, Float) -> (Float, Float) -> Bool
todoMenor x y = fst x < fst y && snd x < snd y

-- la hipotenusa del triángulo rectángulo formado entre ambos puntos
-- (dist x)² + (dist y)² = dist²
-- distancia = sqrt((abs(x1 - x2))² + (abs(y1 - y2))²)
distanciaPuntos :: (Float, Float) -> (Float, Float) -> Float
distanciaPuntos x y = sqrt (abs (fst x - fst y) * abs (fst x - fst y) + abs (snd x - snd y) * abs (snd x - snd y))

sumaTerna :: (Integer, Integer, Integer) -> Integer
sumaTerna (x, y, z) = x + y + z 

-- suma dos numeros solo si son multiplos de un n
sumarSoloMultiplo :: (Integer, Integer) -> Integer -> Integer
sumarSoloMultiplo (a, b) n | a `mod` n == 0 && b `mod` n == 0 = a + b
                           | a `mod` n == 0 = a
                           | b `mod` n == 0 = b
                           | otherwise = 0

sumarSoloMultiplos :: (Integer, Integer, Integer) -> Integer -> Integer
sumarSoloMultiplos (a, b, c) n | n <= 0 = undefined -- quiero que n sea natural
                               | otherwise = sumarSoloMultiplo (sumarSoloMultiplo (a, b) n, c) n

-- asumo que la primera posición es 0, la segunda es 1, etc
posPrimerPar :: (Integer, Integer, Integer) -> Integer
posPrimerPar (a, b, c) | even a = 0
                       | even b = 1
                       | even c = 2
                       | otherwise = 4

crearPar :: a -> b -> (a, b)
crearPar a b = (a, b)

invertir :: (a, b) -> (b, a)
invertir (a, b) = (b, a)

main = do

    -- print (prodInt (1.3, 2.3) (2.3, 5.5)) -- 15.63
    -- print (todoMenor (2.3, 5.5) (5.3, 10.1)) -- True
    -- print (todoMenor (2.3, 15.5) (5.3, 10.1)) -- False
    -- print (todoMenor (7.2, 5.5) (5.3, 10.1)) -- False

    -- print (distanciaPuntos (1, 1) (2, 2)) -- sqrt(2)
    -- print (distanciaPuntos (2, 2) (1, 1)) -- sqrt(2)
    -- print (distanciaPuntos (1, 1) (3, 1)) -- 2
    -- print (distanciaPuntos (0, 0) (16, 16)) -- 16sqrt(2)

    -- print (sumaTerna (1, 2, 3)) -- 6

    -- print (sumarSoloMultiplos (5, 10, 20) 2) -- 30
    -- print (sumarSoloMultiplos (5, 15, 2) 2) -- 2
    -- print (sumarSoloMultiplos (5, 15, 2) 4) -- 0
    -- print (sumarSoloMultiplos (32, 15, 16) 4) -- 48
    -- print (sumarSoloMultiplos (-30,2,12) 3) -- -18

    -- print (posPrimerPar (2, 3, 5)) -- primero
    -- print (posPrimerPar (3, 2, 5)) -- segundo
    -- print (posPrimerPar (3, 5, 2)) -- tercero
    -- print (posPrimerPar (3, 5, 7)) -- 4

    print (crearPar 2.5 "hola") -- (2.5, "hola")
    print (invertir (crearPar 2.5 "hola")) -- ("hola", 2.5)