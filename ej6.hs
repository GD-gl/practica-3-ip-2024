-- Nota: 'not' es la negación
-- not(True) = False, not(False) = True

bisiesto :: Integer -> Bool
bisiesto año = not((año `mod` 4 /= 0) || (año `mod` 100 == 0 && año `mod` 400 /= 0))

main = do
    print (bisiesto 1901) -- False
    print (bisiesto 1900) -- False
    print (bisiesto 1904) -- True
    print (bisiesto 2000) -- True
