# Resoluciones práctica 3 IP 2024 UBA-EXACTAS  
  
Notas:  
- A veces uso funciones brindadas por Haskell como `abs` y `even`. De todas formas son muy simples de implementar.  
- A veces uso la palabra clave 'where' para que el código no sea tan feo y repetitivo  
  