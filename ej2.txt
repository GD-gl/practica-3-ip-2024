a) problema absoluto(n: Z): Z {
    requiere: {True}
    asegura: {res = n <=> n >= 0}
    asegura: {res = -n <=> n < 0}
}

b) problema maximoabsoluto(a: Z, b: Z): Z {
    requiere: {True}
    asegura: {res = absoluto(a) <=> absoluto(a) > absoluto(b)}
    asegura: {res = absoluto(b) <=> absoluto(a) <= absoluto(b)}
}

c) problema maximo3(a: Z, b: Z, c: Z): Z {
    requiere: {True}
    asegura: {res = a <=> a >= b ^ a >= c}
    asegura: {res = b <=> b >= a ^ b >= c}
    asegura: {res = c <=> c >= a ^ b >= c}
}

d) problema algunoEs0(a: R, b: R): Bool {
    requiere: {True}
    asegura: {res = True <=> a == 0 v b == 0}
    asegura: {res = False <=> a != 0 ^ b != 0}
}

e) problema ambosSon0(a: R, b: R): Bool {
    requiere: {True}
    asegura: {res = True <=> a == 0 ^ b == 0}
    asegura: {res = False <=> a != 0 v b != 0}
}

f) problema mismoIntervalo(a: R, b: R): Bool {
    requiere: {True}
    asegura: {res = a,b pertenecen a (-inf, 3] v a,b pertenecen a (3, 7] v a,b pertenecen a (7, +inf)}
}

g) problema sumaDistintos(a: Z, b: Z, c: Z): Z {
    requiere: {True}
    asegura: {res = a + b + c <=> a != b != c}
    asegura: {res = a + b <=> a == c v b == c}
    asegura: {res = a + c <=> a == b v c == b}
    asegura: {res = b + c <=> a == c v a == b}
    asegura: {res = a <=> a == b == c}
}

h) problema esMultiploDe(a: N, b: N): Bool {
    requiere: {True}
    asegura: {res = True <=> a mod b == 0}
    asegura: {res = False <=> a mod b != 0}
}

i) problema digitoUnidades(a: Z): Z {
    requiere: {True}
    asegura: {res = a mod 10}
}

j) problema digitoDecenas(a: Z): Z {
    requiere: {True}
    asegura: {res = piso((a mod 100) / 10)}
}
