-- Obs: x es un entero y (x `div` 10) ya retorna el piso del resultado de la división
-- Obs 2: piso = truncar hacia -inf??? supongo
sumaUltimosDosDigitos :: Integer -> Integer
sumaUltimosDosDigitos x = (abs x `mod` 10) + ((abs x `div` 10) `mod` 10)

-- Obs: uso 'where' para no poner 'sumaUltimosDosDigitos' en todos lados
comparar :: Integer -> Integer -> Integer
comparar a b | sumA < sumB = 1
             | sumA > sumB = -1
             | otherwise = 0
             where sumA = sumaUltimosDosDigitos a
                   sumB = sumaUltimosDosDigitos b

-- sin el where es horrible
compararSinWhere :: Integer -> Integer -> Integer
compararSinWhere a b | sumaUltimosDosDigitos a < sumaUltimosDosDigitos b = 1
                     | sumaUltimosDosDigitos a > sumaUltimosDosDigitos b = -1
                     | otherwise = 0

main = do

    -- print (sumaUltimosDosDigitos 92) -- 11
    -- print (sumaUltimosDosDigitos 1) -- 1
    -- print (sumaUltimosDosDigitos 14) -- 5

    print (comparar 45 312) -- -1
    print (comparar 2312 7) -- 1
    print (comparar 45 172) -- 0
