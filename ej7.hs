-- La especificación puede ser un quilombo pero en realidad es bastante simple
-- res = la sumatoria de la diferencia entre cada elemento de la tupla en la misma posición

distanciaManhattan :: (Float, Float, Float) -> (Float, Float, Float) -> Float
distanciaManhattan (a1, a2, a3) (b1, b2, b3) = abs (a1 - b1) + abs (a2 - b2) + abs (a3 - b3)

main = do
    print (distanciaManhattan (2, 3, 4) (7, 3, 8)) -- 9
    print (distanciaManhattan (-1, 0, -8.5) (3.3, 4, -4)) -- 12.8
