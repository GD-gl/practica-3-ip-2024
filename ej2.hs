absoluto :: Integer -> Integer
absoluto num | num >= 0 = num
             | otherwise = -num

-- uso 'where' para acortar un poco el código
maxabs :: Integer -> Integer -> Integer
maxabs x y | a > b = a
           | otherwise = b
           where a = absoluto x
                 b = absoluto y

-- versión de maxabs sin where
maxabsSW :: Integer -> Integer -> Integer
maxabsSW x y | absoluto x > absoluto y = absoluto x
           | otherwise = absoluto y

-- defino maximo para usarlo en max3
maximo :: Integer -> Integer -> Integer
maximo x y | x > y = x
           | otherwise = y

max3 :: Integer -> Integer -> Integer -> Integer
max3 x y z | x > y = maximo x z
           | y > z = maximo x y
           | otherwise = maximo y z

algunoEs0v1 :: Float -> Float -> Bool
algunoEs0v1 x y | x == 0 = True
                | y == 0 = True
                | otherwise = False

algunoEs0v2 :: Float -> Float -> Bool
algunoEs0v2 x y = x == 0 || y == 0

ambosSon0v1 :: Float -> Float -> Bool
ambosSon0v1 x y | x == 0 = y == 0
                | y == 0 = x == 0
                | otherwise = False

ambosSon0v2 :: Float -> Float -> Bool
ambosSon0v2 x y = x == 0 && y == 0

mismoIntervalo :: Float -> Float -> Bool
mismoIntervalo x y | x <= 3 && y <= 3 = True -- (-inf, 3]
                   | (x <= 7 && y <= 7) && (x > 3 && y > 3) = True -- (3, 7]
                   | x > 7 && y > 7 = True -- (7, +inf)
                   | otherwise = False

-- Ojo!!!: lo que me piden es ambigüo
-- acá asumo que con sumar los no repetidos se refiere a sumar una vez el número y si vuelve a aparecer lo ignoro
-- en cambio en clase se definió como 0 si x == y == z, ignorando totalmente a los repetidos
sumaDistintos :: Integer -> Integer -> Integer -> Integer
sumaDistintos x y z | x == y && y == z = x -- todos iguales
                    | x == y = x + z -- x = y
                    | y == z = x + z -- y = z
                    | x == z = x + y -- x = z
                    | otherwise = x + y + z -- todos distintos

esMultiploDe :: Integer -> Integer -> Bool
esMultiploDe x y | x <= 0 || y <= 0 = undefined -- no son naturales
                 | otherwise = mod x y == 0

-- Obs: acá inicialmente no me dí cuenta que tendría que haber usado valores absolutos (casos negativos)
digitoUnidades :: Integer -> Integer
digitoUnidades x = absoluto x `mod` 10

-- Obs: acá inicialmente no me dí cuenta que tendría que haber usado valores absolutos (casos negativos)
-- Obs 2: el primer parámetro de div no puede ser negativo (no tengo que pensar en el truncamiento hacia -inf)
digitoDecenas :: Integer -> Integer
digitoDecenas x = (absoluto x `mod` 100) `div` 10

main = do
    -- print (absoluto 10)
    -- print (absoluto (-5))

    -- print (maxabs (-5) 10) -- 10
    -- print (maxabs (-5) (-10)) -- 10
    -- print (maxabs (-20) (-10)) -- 20
    -- print (maxabs 20 (-10)) -- 20

    -- print (max3 10 20 30) -- 30
    -- print (max3 10 30 20) -- 30
    -- print (max3 30 10 20) -- 30

    -- print (algunoEs0v1 5.3 0.0) -- True
    -- print (algunoEs0v1 0.0 5.3) -- True
    -- print (algunoEs0v1 5.3 (-3.2)) -- False
    -- print (algunoEs0v2 5.3 0.0) -- True
    -- print (algunoEs0v2 0.0 5.3) -- True
    -- print (algunoEs0v2 5.3 (-3.2)) -- False

    -- print (ambosSon0v1 0.0 0.0) -- True
    -- print (ambosSon0v1 0.0 1.0) -- False
    -- print (ambosSon0v1 1.0 0.0) -- False
    -- print (ambosSon0v2 0.0 0.0) -- True
    -- print (ambosSon0v2 0.0 1.0) -- False
    -- print (ambosSon0v2 1.0 0.0) -- False

    -- print (mismoIntervalo (-27832.4233) 3) -- True
    -- print (mismoIntervalo 7 5) -- True
    -- print (mismoIntervalo 79 32) -- True
    -- print (mismoIntervalo 2 4) -- False
    -- print (mismoIntervalo 7 8) -- False
    -- print (mismoIntervalo (-2) 8) -- False

    -- print (sumaDistintos 10 20 30) -- 60
    -- print (sumaDistintos 10 20 20) -- 30
    -- print (sumaDistintos 10 0 20) -- 30
    -- print (sumaDistintos 10 10 3) -- 13
    -- print (sumaDistintos 1 2 3) -- 6
    -- print (sumaDistintos (-5) 20 (-5)) -- 15

    -- print (esMultiploDe (-1) 2) -- undefined
    -- print (esMultiploDe 2 4) -- False
    -- print (esMultiploDe 4 2) -- True

    -- print (digitoUnidades 14) -- 4
    -- print (digitoUnidades 24232) -- 2
    -- print (digitoUnidades (-1)) -- 1
    -- print (digitoDecenas 10) -- 1
    -- print (digitoDecenas 92834) -- 3
    -- print (digitoDecenas 0) -- 0

    print "Hecho"
