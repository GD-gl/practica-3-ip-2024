-- acá uso la función "even" brindada por haskell
-- even es básicamente "n mod 2 == 0"

f :: Integer -> Integer
f n | n <= 7 = n * n
    | otherwise = 2 * n - 1

g :: Integer -> Integer
g n | even n = n `div` 2
    | otherwise = 3 * n + 1

todosMenores :: (Integer, Integer, Integer) -> Bool
todosMenores (a, b, c) = (f a > g a) && (f b > g b) && (f c > g c)

main = do
    print (todosMenores (1, 2, 3)) -- False
    print (todosMenores (10, 2, 3)) -- False
    print (todosMenores (6, 7, 6)) -- True
    print (todosMenores (7, 7, 7)) -- True
    print (todosMenores (5, 7, 4)) -- True
